function sample_out=run_dsp_simulation(input_source,    ...
				       output_filename, ...
				       main,            ...
				       init_data)
%
%  run_dsp_simulation(input_filename, output_filename, main, init_data)
%
  %
  % Parse call
  %
  if nargin < 3
    error('Usage: run_dsp_simulation(input, output, main, [init])')
  end
  
  if nargin < 4
    init_data = [];
  end

  %
  % Read input source
  %
  if isa(input_source, 'char')
    [input, msg] = fopen(input_source, 'r');
    
    if input < 0
      error('Error while opening "%s" in input: %s', ...
	    input_filename,                          ...
	    msg);
    end

    sample_in=fscanf(input, '%d', inf);
    fclose(input);

  elseif isa(input_source, 'numeric')
    sample_in=input_source;

  else
    error('INPUT_SOURCE must be a filename or an array, not a %s', ...
	  class(input_source));
  end

  %
  % Prepare output destination
  %
  if ~isempty(output_filename)
    [output, msg] = fopen(output_filename, 'w');
    
    if output < 0
      error('Error while opening "%s" in output: %s', ...
	    output_filename, ...
	    msg);
    end

  else
    output = [];

  end

  %
  % MAIN 
  %
  sample_out = [];

  main('start', init_data)
  counter = 0;
  
  for n=1:length(sample_in)
    adc(int16(sample_in(n)));
    dac('clear');
    
    main('run', @adc, @dac, n-1);

    y = dac();

    if ~isempty(y)
      sample_out = [sample_out y];
    end
  end

  main('stop')

  if ~isempty(output)
    fprintf(output, '%d\n', sample_out);
    fclose(output)
  end
  
  
  function result=adc(sample)
    %
    % If this function is called with one parameter
    % the value of sample is stored in a persistent
    % "register;"  if the function is called without parameters,
    % it returns the value stored in the register
    %
    
    persistent mem

    if nargin == 0
      result=mem;

    elseif nargin == 1
      if ~strcmp('int16', class(sample))
	error('Bad value type: %s instead of int16', class(sample));
      end
      
      mem=sample;
      result=[];
      
    else
      error('Bingo bongo in 3/4')
    end
  
  function result=dac(sample)
    %
    % If this function is called with one parameter
    % the value of sample is stored in a persistent
    % "register;"  if the function is called without parameters,
    % it returns the value stored in the register
    %
    % Yes, it works like adc; indeed, they need to do the same thing
    %
    
    persistent mem

    if nargin == 0
      result=mem;
      return
    end

    result = [];
    
    if isa(sample, 'char')
      command=sample;
      
      switch command
	case 'clear'
	  mem=[];

	otherwise
	  error('Urecognized DAC command "%s"', command)
      end

      return
    end

    if ~strcmp('int16', class(sample))
      error('Bad value type: %s instead of int16', class(sample));
    end
      
    mem(end+1)=sample;
  
