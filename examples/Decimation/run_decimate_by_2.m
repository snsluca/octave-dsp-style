addpath ../../src

result = run_dsp_simulation('input.txt', ...  % Read data from file
			    [],          ...  % No output file
			    @decimate_by_2)   % virtual handler
                                              % No INIT_DATA required
