# What is this?

This directory contains `decimate_by_2.m` which is an example of virtual
handler that decimates its input by a factor of 2.  It is an example
of usage of the CLOCK parameter.

You can run the example by setting this directory to the octave current
directory and calling `run_decimate_by_2`.
