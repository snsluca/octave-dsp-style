function linear_inter(command, adc, dac, ck)

  %
  % This "virtual handler" interpolates its input by a factor of 2
  % using linear interpolation; in other words, if x(n) and x(n+1) are
  % two consecutive input samples, the additional sample produced will
  % be (x(n)+x(n-1))/2
  %
  
  %
  % handler state: we need only to remember the previous sample
  %
  persistent old_input  
  
  switch command
    case 'start'
      %
      % Boot time: setup the internal state
      %
      old_input = 0;
      return
 
    case 'stop'
      %
      % No cleanup required
      %
      return;
     
    case 'run'
      current_sample = double(adc());

      intermediate = (old_input + current_sample) / 2;
  

      dac(int16(intermediate));
      dac(int16(current_sample));

      old_input = current_sample;
      
      return;

    otherwise
      %
      % We should never arrive here, but let's play safe
      %
      error('Unrecognized command "%s"', command);

  end

