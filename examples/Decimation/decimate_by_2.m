function decimate_by_2(command, adc, dac, ck)

  %
  % This "virtual handler" decimate its input by a factor of 2, using as
  % prefilter H(z) = (1+z^-1)/2, that is, it takes the average
  % of the current sample and the previous one
  %
  
  %
  % handler state: we need only to remember the previous sample
  %
  persistent old_input
  
  switch command
   case 'start'
      %
      % Boot time: setup the internal state
      %
      old_input = 0;
      return
 
    case 'stop'
      %
      % No cleanup required
      %
      return;
     
    case 'run'
      current_sample = double(adc());

      if mod(ck, 2) == 1
        %
        % Current time is odd -> It is not "output time."
        % Save the current sample for later
        %
        old_input = current_sample;

      else
        %
        % Current time is even -> Output the average between
        % the current sample and the old one
        %
        dac(int16((old_input + current_sample) / 2));

      end
    
      return
    
    otherwise
      %
      % We should never arrive here, but let's play safe
      %
      error('Unrecognized command "%s"', command);

  end

