function smallest_iir(command, adc, dac, ck)

  %
  % This "main" implements the smallest IIR filter
  %
  %   y(n) = A * y(n-1) + x(n)
  %
  % INIT_DATA must be a scalar with the value of coefficient A
  %

  % ---------------------------------------------------------

  %
  % In Octave/Matlab variables that are declared "persistent"
  % mantain their value even after the function returns to
  % the caller.  We use it to keep the filter status: the old
  % output and the filter coefficient A
  %
  persistent old_output coeff
  
  switch command
    case 'start'
      %
      % Here we are at boot time. INIT_DATA is the second parameter
      % (named adc).  Here we copy it into a variable init_data
      % for clarity
      %
      init_data=adc;

      %
      % INIT_DATA is the coefficient A of the IIR.  Therefore,
      % it must be a numeric scalar
      %
      if ~ (isa(init_data, 'float') && prod(size(init_data)) == 1)
	       error('Init_data is not a scalar')
      end

      %
      % Initialize the state variables
      %
      coeff      = init_data; % Save coefficient A for later
      old_output = 0.0;       % Start with filter "empty"
      return

    case 'stop'
      %
      % We are shutdowning. Nothing to do here. 
      %
      return;


    case 'run'
      %
      % "Normal: case: a new input sample is ready.
      %
      input = double(adc());              % Read the current sample x(n)

      result = coeff*old_output + input;  % Compute y(n)=A*y(n-1)+x(n)
  
      old_output = result;                % Save y(n) for later

      dac(int16(result));                 % Send y(n) to the DAC
  
      return;
      
    otherwise
      %
      % We should never arrive here, but let's play safe
      %
      error('Unrecognized command "%s"', command);

  end

      
