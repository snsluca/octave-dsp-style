# Octave DSP–style

## What is this?

This is a small octave library that I wrote to allow my students to get some experience with writing signal processing algorithm in "DSP style," that is by processing one sample at time, in octave.

The main difference between doing off–line signal processing with octave and doing it on a DSP, it is that in octave the input signal is (usually) fully available in some vector, while in a DSP the signal arrives from the ADC (Analog to Digital Converter) "one sample at the time"  and this requires quite different approaches.

In my opinion, a DSP course should include some experience both with the "off–line model" (that can be done in octave) and both with the "DSP model" (that requires a DSP card). However, working with a DSP card introduces a whole sets of additional difficulties: from working with a non specialized language like C up to taking care of low–level details like pin configuration.

With the help of this library it is possible to program with the DSP model while 
working in the convenient ambient provided by octave. 

### The model

The library is inspired to the following model: we have a card with a DSP onboard. An ADC samples regularly the input signal and when a conversion is done it raises an interrupt to the DSP. The interrupt handler reads the new sample and processes it,  producing one (or more) output samples that are sent to a DAC (Digital to Analog Converter).  In some cases it can also be that no output sample is produced, but the input sample is stored for later processing (e.g., if some decimation is done).

The card sends to the DSP  one "startup" signal at boot time and one "stop" signal at shutdown. They can be used to do some initialization and cleanup.

With this library the user just needs to provide a *virtual interrupt handler* in the form of a `.m` file. The type of "signal" that the "virtual handler" must handle is given (as a string) to the handler in the first parameter: `'start'` at "virtual boot time," `'run'` when a new sample is virtually acquired and `'stop'` at "virtual shutdown time."


### Any examples?

Sure, look into the `examples/` directory

## Installation

Pretty simple: copy the file `run_dsp_simulation.m` (that you find under the `src/` directory) where octave (or matlab) will find it.

## Usage

### Overview 

* The user writes an octave function that plays the role of the *virtual handler*
* The virtual handler is given to the  *driver function*  `run_dsp_simulation` that will take care of everything (e.g., I/O, start–up and shutdown, ...)


### The user *virtual handler*

The user virtual handler will be called by the driver function `run_dsp_simulation` in three cases

* One time at boot time to allow the virtual handler to initialize its internal state.
* One time for every input sample
* One time after all samples have been processed to allow the handler to do any "final clean up" if necessary.

### Virtual handler interface

The user provided virtual handler (called  `main` in the code examples)  must implement the interface described in this section.

#### Boot 

At start up the virtual handler `main` is called as follows
```matlab
  main('start', init_data);
``` 

where `init_data` is a parameter given to the driver `run_dsp_simulation` and it allows to pass parameters to the virtual handler at initialization time.

See `smallest_iir.m` in `examples/` for an example of usage of `init_data`

#### Shut–down

Before shut down the virtual handler is called as
```matlab
  main('stop');
```

#### After each sample

At every input sample the virtual handler is called as

```matlab
  main('run', ADC, DAC, CLOCK)
```
where
* `ADC` is an *octave function handle* to be called as `ADC()` (**NOTE**: for technical reasons the parenthesis are **mandatory** here). `ADC()` returns the current input sample as an `int16`
* `DAC` is an octave function handle to be called as `DAC(sample)` where `sample` is an output sample with type `int16`. If the function is called more than once during a callback, all the samples will be output in the same order. If it is never called during a specific callback, no output sample will be produced. See `decimate_by_2.m` and `linear_inter.m` in `examples/Interpolation/`
* `CLOCK` is an integer value representing the current "normalized time."  `CLOCK` starts from 0 and it is incremented at every call.  Useful, for example, when doing subsampling.  See `decimate_by_2.m` in `examples/Decimation/`

### Running your code

The virtual handler is called via the *driver* function `run_dsp_simulation` which needs

1. a source for input samples
2. a destination for output samples
3. a reference to the user provided virtual handler (you can get the reference using the octave `@`–syntax, e.g., `@main`)
4. optionally, some *opaque data* to be given at the virtual handler at boot time. 

> "Opaque" because the data are given "as they are" to the virtual handler. The function `run_dsp_simulation` does not care about it.

#### Interface of  `run_dsp_simulation`

More into details, the syntax for calling `run_dsp_simulation` is the following

```matlab
  result=run_dsp_simultation(input_source, output, @main, init_data);
```


where
* `input_source` can be
   * The  **name of a file** with the input samples in text format.  Every sample is an integer in the range –2^15 .. 2^15–1, that is, a 16 bit signed integer.
   * A **vector** that contains the input samples.  The vector must be of numeric type (integer or float) and it will be internally converted to `int16`
* `output` can be
  * The **name of the file** where the output will be written in text format (still as 16 bit integers)
  * The **empty string** or an **empty matrix**. In this case the output is not written anywhere and only returned as result from the function
* `@main` is the *octave handle* to an user provided function that implements the "virtual interrupt handler".
* `init_data` is *optional* and it defaults to an empty matrix.  It is given to `main` at boot time.
* `result` is a vector with the output samples produced by the function `main`


`run_dsp_simulation` will take care of initializing the virtual handler `main`, call it for every input sample and call it at shutdown time.

